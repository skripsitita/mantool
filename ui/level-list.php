<!DOCTYPE html>
<html lang="en">
 <head>
  <title>Users List - Project Management</title>
  <!-- HTML5 Shim and Respond.js IE10 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 10]>
   <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
   <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
  <![endif]-->
  <!-- Meta -->
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="description" content="#" />
  <meta name="keywords" content="Admin , Responsive, Landing, Bootstrap, App, Template, Mobile, iOS, Android, apple, creative app" />
  <meta name="author" content="#" />
  <!-- Favicon icon -->
  <link rel="icon" href="assets\images\favicon.ico" type="image/x-icon" />
  <!-- Google font-->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600" rel="stylesheet" />
  <!-- Required Fremwork -->
  <link rel="stylesheet" type="text/css" href="components\bootstrap\css\bootstrap.min.css" />
  <!-- feather Awesome -->
  <link rel="stylesheet" type="text/css" href="assets\icon\feather\css\feather.css" />
  <!-- Style.css -->
  <link rel="stylesheet" type="text/css" href="assets\css\style.css" />
  <link rel="stylesheet" type="text/css" href="assets\css\custom_l9.css" />
  <link rel="stylesheet" type="text/css" href="assets\css\jquery.mCustomScrollbar.css" />
 </head>

 <body>
  <!-- Pre-loader start -->
  <div class="theme-loader">
   <div class="ball-scale">
    <div class="contain">
     <div class="ring">
      <div class="frame"></div>
     </div>
     <div class="ring">
      <div class="frame"></div>
     </div>
     <div class="ring">
      <div class="frame"></div>
     </div>
     <div class="ring">
      <div class="frame"></div>
     </div>
     <div class="ring">
      <div class="frame"></div>
     </div>
     <div class="ring">
      <div class="frame"></div>
     </div>
     <div class="ring">
      <div class="frame"></div>
     </div>
     <div class="ring">
      <div class="frame"></div>
     </div>
     <div class="ring">
      <div class="frame"></div>
     </div>
     <div class="ring">
      <div class="frame"></div>
     </div>
    </div>
   </div>
  </div>
  <!-- Pre-loader end -->
  <div id="pcoded" class="pcoded">
   <div class="pcoded-overlay-box"></div>
   <div class="pcoded-container navbar-wrapper">
    <?php include 'includes/nav.inc.php';?>

    <!-- Sidebar inner chat end-->
    <div class="pcoded-main-container">
     <div class="pcoded-wrapper">
      <?php include 'includes/sidebar.inc.php';?>
      <div class="pcoded-content">
       <div class="pcoded-inner-content">
        <div class="main-body">
         <div class="page-wrapper">
          <div class="page-header">
           <div class="row align-items-end">
            <div class="col-lg-10">
             <div class="page-header-title">
              <div class="d-inline">
               <h4>Users Level List</h4>
              </div>
             </div>
            </div>
            <div class="col-lg-2">
             <a href="add-users.php" class="btn btn-block btn-mat btn-primary"><i class="feather icon-plus-square"></i>Add New</a>
            </div>
           </div>
          </div>
          <div class="page-body">
           <div class="row">
            <div class="col-xl-12 col-md-12">
             <div class="card table-card">
              <div class="card-header">
               <h5>Users  Level List</h5>
              </div>
              <div class="card-block">
               <div class="table-responsive">
                <table class="table table-hover table-bordered">
                 <thead>
                  <tr>
                   <th>#</th>
                   <th>Level Name</th>
                   <th>Status</th>
                   <th>Action</th>
                  </tr>
                 </thead>
                 <tbody>
                  <tr>
                   <td>
                    1
                   </td>

                     <td>
                    Administrator
                   </td>

                
                   <td><label class="label label-success">Active</label></td>
                   <td>
                    <a href="" class="ic_action act_edit"><i class="feather icon-edit-2"></i></a>

                    <a href="" class="ic_action act_delete"><i class="feather icon-delete"></i></a>
                   </td>
                  </tr>

                   <tr>
                   <td>
                    2
                   </td>

                      <td>
                    Officer
                   </td>

                
                   <td><label class="label label-success">Active</label></td>
                   <td>
                    <a href="" class="ic_action act_edit"><i class="feather icon-edit-2"></i></a>

                    <a href="" class="ic_action act_delete"><i class="feather icon-delete"></i></a>
                   </td>
                  </tr>

                  <tr>
                   <td>
                   3
                   </td>

                  
                   <td>
                    Directur
                   </td>
                   <td><label class="label label-success">Active</label></td>
                   <td>
                    <a href="" class="ic_action act_edit"><i class="feather icon-edit-2"></i></a>

                    <a href="" class="ic_action act_delete"><i class="feather icon-delete"></i></a>
                   </td>
                  </tr>
                 </tbody>
                </table>
               </div>
              </div>
             </div>
            </div>
           </div>
          </div>
         </div>
        </div>
       </div>
      </div>
     </div>
    </div>
   </div>
  </div>

  <!-- Warning Section Ends -->
  <!-- Required Jquery -->
  <script type="text/javascript" src="components\jquery\js\jquery.min.js"></script>
  <script type="text/javascript" src="components\jquery-ui\js\jquery-ui.min.js"></script>
  <script type="text/javascript" src="components\popper.js\js\popper.min.js"></script>
  <script type="text/javascript" src="components\bootstrap\js\bootstrap.min.js"></script>
  <!-- jquery slimscroll js -->
  <script type="text/javascript" src="components\jquery-slimscroll\js\jquery.slimscroll.js"></script>
  <!-- modernizr js -->
  <script type="text/javascript" src="components\modernizr\js\modernizr.js"></script>
  <!-- amchart js -->
  
  <script src="assets\js\jquery.mCustomScrollbar.concat.min.js"></script>
  <script type="text/javascript" src="assets\js\SmoothScroll.js"></script>
  <script src="assets\js\pcoded.min.js"></script>
  <!-- custom js -->
  <script src="assets\js\vartical-layout.min.js"></script>
  <script type="text/javascript" src="assets\js\script.min.js"></script>
 </body>
</html>
