<!DOCTYPE html>
<html lang="en">
 <head>
  <title>Add User- Project Management</title>
  <!-- HTML5 Shim and Respond.js IE10 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 10]>
   <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
   <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
  <![endif]-->
  <!-- Meta -->
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="description" content="#" />
  <meta name="keywords" content="Admin , Responsive, Landing, Bootstrap, App, Template, Mobile, iOS, Android, apple, creative app" />
  <meta name="author" content="#" />
  <!-- Favicon icon -->
  <link rel="icon" href="assets\images\favicon.ico" type="image/x-icon" />
  <!-- Google font-->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600" rel="stylesheet" />
  <!-- Required Fremwork -->
  <link rel="stylesheet" type="text/css" href="components\bootstrap\css\bootstrap.min.css" />
  <!-- feather Awesome -->
  <link rel="stylesheet" type="text/css" href="assets\icon\feather\css\feather.css" />
  <!-- Select 2 css -->
  <link rel="stylesheet" href="components\select2\css\select2.min.css" />
  <!-- Multi Select css -->
  <link rel="stylesheet" type="text/css" href="components\bootstrap-multiselect\css\bootstrap-multiselect.css" />
  <link rel="stylesheet" type="text/css" href="components\multiselect\css\multi-select.css" />
  <!--forms-wizard css-->
  <link rel="stylesheet" type="text/css" href="components\jquery.steps\css\jquery.steps.css" />
  <!-- Style.css -->
  <link rel="stylesheet" type="text/css" href="assets\css\style.css" />
  <link rel="stylesheet" type="text/css" href="assets\css\custom_l9.css" />
  <link rel="stylesheet" type="text/css" href="assets\css\jquery.mCustomScrollbar.css" />
 </head>

 <body>
  <!-- Pre-loader start -->
  <div class="theme-loader">
   <div class="ball-scale">
    <div class="contain">
     <div class="ring">
      <div class="frame"></div>
     </div>
     <div class="ring">
      <div class="frame"></div>
     </div>
     <div class="ring">
      <div class="frame"></div>
     </div>
     <div class="ring">
      <div class="frame"></div>
     </div>
     <div class="ring">
      <div class="frame"></div>
     </div>
     <div class="ring">
      <div class="frame"></div>
     </div>
     <div class="ring">
      <div class="frame"></div>
     </div>
     <div class="ring">
      <div class="frame"></div>
     </div>
     <div class="ring">
      <div class="frame"></div>
     </div>
     <div class="ring">
      <div class="frame"></div>
     </div>
    </div>
   </div>
  </div>
  <!-- Pre-loader end -->
  <div id="pcoded" class="pcoded">
   <div class="pcoded-overlay-box"></div>
   <div class="pcoded-container navbar-wrapper">
    <?php include 'includes/nav.inc.php';?>

    <!-- Sidebar inner chat end-->
    <div class="pcoded-main-container">
     <div class="pcoded-wrapper">
      <?php include 'includes/sidebar.inc.php';?>
      <div class="pcoded-content">
       <div class="pcoded-inner-content">
        <div class="main-body">
         <div class="page-wrapper">
          <div class="page-header">
           <div class="row align-items-end">
            <div class="col-lg-10">
             <div class="page-header-title">
              <div class="d-inline">
               <h4>Add New Project</h4>
              </div>
             </div>
            </div>
            <div class="col-lg-2">
             <a href="users-list.php" class="btn btn-block btn-mat btn-primary"><i class="feather icon-list"></i>Project List</a>
            </div>
           </div>
          </div>
          <div class="page-body">
           <div class="row">
            <div class="col-md-12">
             <div id="wizard">
              <section>
               <form class="wizard-form" id="example-advanced-form" action="#">
                <h3>Project</h3>
                <fieldset>
                 <div class="form-group row">
                  <label class="col-sm-2 col-form-label">Project No <small class="txt_mandatory">(*)</small></label>
                  <div class="col-sm-4">
                   <input type="text" class="form-control" name="name" id="name" placeholder="PRJ-0012020" readonly="" />
                   <span class="messages"></span>
                  </div>
                 </div>

                 <div class="form-group row">
                  <label class="col-sm-2 col-form-label">Client Name<small class="txt_mandatory">(*)</small></label>
                  <div class="col-sm-6">
                   <select id="hello-single" class="form-control">
                    <option value="0">-- Select One -- </option>
                    <option value="1">PT Indosurya Finance</option>
                    <option value="2">Fremantle </option>
                   </select>
                   <span class="messages"></span>
                  </div>
                 </div>

                 <div class="form-group row">
                  <label class="col-sm-2 col-form-label">Project Category<small class="txt_mandatory">(*)</small></label>
                  <div class="col-sm-5">
                   <select id="hello-single" class="form-control">
                    <option value="0">-- Select One -- </option>
                    <option value="1">Company Profile</option>
                    <option value="2">Fintech </option>
                    <option value="3">Eccommerce </option>
                   </select>
                   <span class="messages"></span>
                  </div>
                 </div>

                 <div class="form-group row">
                  <label class="col-sm-2 col-form-label">Project Type<small class="txt_mandatory">(*)</small></label>
                  <div class="col-sm-5">
                   <select id="hello-single" class="form-control">
                    <option value="0">-- Select One -- </option>
                    <option value="1">Website</option>
                    <option value="2">Mobile </option>
                    <option value="3">Desktop </option>
                   </select>
                   <span class="messages"></span>
                  </div>
                 </div>

                 <div class="form-group row">
                  <label class="col-sm-2 col-form-label">Project Name <small class="txt_mandatory">(*)</small></label>
                  <div class="col-sm-6">
                   <input type="text" class="form-control" name="name" id="name" placeholder="e.g. Indonesian Idol" />
                   <span class="messages"></span>
                  </div>
                 </div>

                 <div class="form-group row">
                  <label class="col-sm-2 col-form-label">Project Value <small class="txt_mandatory">(*)</small></label>
                  <div class="col-sm-5">
                   <div class="input-group m-b-0">
                    <span class="input-group-addon" id="basic-addon2">Rp</span>
                    <input type="text" class="form-control" placeholder="e.g 10.000.000" />
                   </div>
                  </div>
                 </div>

                 <div class="row form-group">
                  <div class="col-md-6">
                   <div class="row">
                    <label class="col-sm-4 col-form-label">Project Start <small class="txt_mandatory">(*)</small></label>
                    <div class="col-sm-6">
                     <input class="form-control" type="date" />
                    </div>
                   </div>
                  </div>

                  <div class="col-md-6">
                   <div class="row">
                    <label class="col-sm-4 col-form-label">Project End <small class="txt_mandatory">(*)</small></label>
                    <div class="col-sm-6">
                     <input class="form-control" type="date" />
                    </div>
                   </div>
                  </div>
                 </div>

                 <div class="form-group row">
                  <label class="col-sm-2 col-form-label">Duration<small class="txt_mandatory">(*)</small></label>
                  <div class="col-sm-3">
                   <div class="input-group m-b-0">
                    <input type="text" class="form-control" placeholder="e.g 30" />
                    <span class="input-group-addon" id="basic-addon2">Days</span>
                   </div>
                  </div>
                 </div>

                 <div class="form-group row">
                  <label class="col-sm-2 col-form-label">Status<small class="txt_mandatory">(*)</small></label>
                  <div class="col-sm-5">
                   <select id="hello-single" class="form-control">
                    <option value="0">-- Select One -- </option>
                    <option value="1">Preparation</option>
                    <option value="2">On Going </option>
                    <option value="3">On Monitoring </option>
                    <option value="3">Done </option>
                    <option value="3">Pending </option>
                   </select>
                   <span class="messages"></span>
                  </div>
                 </div>
                </fieldset>

                <h3>Assign Officer</h3>
                <fieldset>
                 <table class="table table-hover table-bordered">
                  <thead>
                   <tr>
                    <th>#</th>
                    <th>Officer Name</th>
                    <th>Job Desc</th>
                   </tr>
                  </thead>
                  <tbody>
                   <tr>
                    <td>
                     1
                    </td>

                    <td>
                     <select class="form-control required">
                      <option>-- Select One-- </option>
                      <option>Nur Fauzi (NF)</option>
                      <option>Tita Aprilianti (TA)</option>
                      <option>Zainu Rochim (ZR)</option>
                      <option>Galih Kusuma (GK)</option>
                      <option>Iyu Priatna (IP)</option>
                      <option>Fajar Nur Iman (FN)</option>
                      <option>Yosi Yuniar (YY)</option>
                      <option>Iwan Kurniawan (IK)</option>
                      <option>Mega Murdiana (MM)</option>
                     </select>
                    </td>

                    <td>
                     <input type="text" class="form-control" name="name" id="name" placeholder="e.g. Database Engineer " />
                    </td>
                   </tr>

                   <tr>
                    <td>
                     2
                    </td>

                    <td>
                     <select class="form-control required">
                      <option>-- Select One-- </option>
                      <option>Nur Fauzi (NF)</option>
                      <option>Tita Aprilianti (TA)</option>
                      <option>Zainu Rochim (ZR)</option>
                      <option>Galih Kusuma (GK)</option>
                      <option>Iyu Priatna (IP)</option>
                      <option>Fajar Nur Iman (FN)</option>
                      <option>Yosi Yuniar (YY)</option>
                      <option>Iwan Kurniawan (IK)</option>
                      <option>Mega Murdiana (MM)</option>
                     </select>
                    </td>

                    <td>
                     <input type="text" class="form-control" name="name" id="name" placeholder="e.g. UI/UX Developer " />
                    </td>
                   </tr>
                  </tbody>
                 </table>
                 <p class="text-right">
                  <a href="users-list.php" class="btn btn-mat btn-primary"><i class="feather icon-plus"></i>Add New</a>
                 </p>
                </fieldset>
                <h3>Modul</h3>
                <fieldset>
                 <table class="table table-hover table-bordered">
                  <thead>
                   <tr>
                    <th style="width: 10px;">#</th>
                    <th style="width: 10px;">
                     <div class="border-checkbox-group border-checkbox-group-primary">
                      <input class="border-checkbox" type="checkbox" id="checkbox1" />
                      <label class="border-checkbox-label" for="checkbox1"></label>
                     </div>
                    </th>
                    <th>Item Name</th>
                   </tr>
                  </thead>
                  <tbody>
                   <tr>
                    <td>
                     1
                    </td>

                    <td>
                     <div class="border-checkbox-group border-checkbox-group-primary">
                      <input class="border-checkbox" type="checkbox" id="checkbox1" />
                      <label class="border-checkbox-label" for="checkbox1"></label>
                     </div>
                    </td>

                    <td>
                     <input type="text" class="form-control" name="name" id="name" placeholder="e.g. Login Log Out " />
                    </td>
                   </tr>

                   <tr>
                    <td>
                     2
                    </td>

                    <td>
                     <div class="border-checkbox-group border-checkbox-group-primary">
                      <input class="border-checkbox" type="checkbox" id="checkbox1" />
                      <label class="border-checkbox-label" for="checkbox1"></label>
                     </div>
                    </td>

                    <td>
                     <input type="text" class="form-control" name="name" id="name" placeholder="e.g. Slider " />
                    </td>
                   </tr>
                  </tbody>
                 </table>
                 <p class="text-right">
                  <a href="users-list.php" class="btn btn-mat btn-primary"><i class="feather icon-plus"></i>Add Item</a>
                 </p>
                </fieldset>
               </form>
              </section>
             </div>
            </div>
           </div>
          </div>
         </div>
        </div>
       </div>
      </div>
     </div>
    </div>
   </div>
  </div>

  <!-- Required Jquery -->
  <script type="text/javascript" src="components\jquery\js\jquery.min.js"></script>
  <script type="text/javascript" src="components\jquery-ui\js\jquery-ui.min.js"></script>
  <script type="text/javascript" src="components\popper.js\js\popper.min.js"></script>
  <script type="text/javascript" src="components\bootstrap\js\bootstrap.min.js"></script>
  <!-- jquery slimscroll js -->
  <script type="text/javascript" src="components\jquery-slimscroll\js\jquery.slimscroll.js"></script>

  <!-- modernizr js -->
  <script type="text/javascript" src="components\modernizr\js\modernizr.js"></script>
  <script type="text/javascript" src="components\modernizr\js\css-scrollbars.js"></script>

  <!-- i18next.min.js -->
  <script type="text/javascript" src="components\i18next\js\i18next.min.js"></script>
  <script type="text/javascript" src="components\i18next-xhr-backend\js\i18nextXHRBackend.min.js"></script>
  <script type="text/javascript" src="components\i18next-browser-languagedetector\js\i18nextBrowserLanguageDetector.min.js"></script>
  <script type="text/javascript" src="components\jquery-i18next\js\jquery-i18next.min.js"></script>
  <!-- Select 2 js -->
  <script type="text/javascript" src="components\select2\js\select2.full.min.js"></script>
  <!-- Multiselect js -->
  <script type="text/javascript" src="components\bootstrap-multiselect\js\bootstrap-multiselect.js"></script>
  <script type="text/javascript" src="components\multiselect\js\jquery.multi-select.js"></script>
  <script type="text/javascript" src="assets\js\jquery.quicksearch.js"></script>

  <!-- Bootstrap date-time-picker js -->
  <script type="text/javascript" src="assets\pages\advance-elements\moment-with-locales.min.js"></script>

  <!--Forms - Wizard js-->
  <script src="components\jquery.cookie\js\jquery.cookie.js"></script>
  <script src="components\jquery.steps\js\jquery.steps.js"></script>
  <script src="components\jquery-validation\js\jquery.validate.js"></script>
  <!-- Validation js -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.8.3/underscore-min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/moment.min.js"></script>
  <script type="text/javascript" src="assets\pages\form-validation\validate.js"></script>

  <!-- Custom js -->
  <script src="assets\pages\advance-elements\select2-custom.js"></script>
  <script src="assets\pages\forms-wizard-validation\form-wizard.js"></script>
  <script src="assets\js\pcoded.min.js"></script>
  <script src="assets\js\vartical-layout.min.js"></script>
  <script src="assets\js\jquery.mCustomScrollbar.concat.min.js"></script>
  <script type="text/javascript" src="assets\js\script.js"></script>
 </body>
</html>
