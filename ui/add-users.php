<!DOCTYPE html>
<html lang="en">
 <head>
  <title>Add User- Project Management</title>
  <!-- HTML5 Shim and Respond.js IE10 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 10]>
   <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
   <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
  <![endif]-->
  <!-- Meta -->
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="description" content="#" />
  <meta name="keywords" content="Admin , Responsive, Landing, Bootstrap, App, Template, Mobile, iOS, Android, apple, creative app" />
  <meta name="author" content="#" />
  <!-- Favicon icon -->
  <link rel="icon" href="assets\images\favicon.ico" type="image/x-icon" />
  <!-- Google font-->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600" rel="stylesheet" />
  <!-- Required Fremwork -->
  <link rel="stylesheet" type="text/css" href="components\bootstrap\css\bootstrap.min.css" />
  <!-- feather Awesome -->
  <link rel="stylesheet" type="text/css" href="assets\icon\feather\css\feather.css" />
  <!-- Style.css -->
  <link rel="stylesheet" type="text/css" href="assets\css\style.css" />
  <link rel="stylesheet" type="text/css" href="assets\css\custom_l9.css" />
  <link rel="stylesheet" type="text/css" href="assets\css\jquery.mCustomScrollbar.css" />
 </head>

 <body>
  <!-- Pre-loader start -->
  <div class="theme-loader">
   <div class="ball-scale">
    <div class="contain">
     <div class="ring">
      <div class="frame"></div>
     </div>
     <div class="ring">
      <div class="frame"></div>
     </div>
     <div class="ring">
      <div class="frame"></div>
     </div>
     <div class="ring">
      <div class="frame"></div>
     </div>
     <div class="ring">
      <div class="frame"></div>
     </div>
     <div class="ring">
      <div class="frame"></div>
     </div>
     <div class="ring">
      <div class="frame"></div>
     </div>
     <div class="ring">
      <div class="frame"></div>
     </div>
     <div class="ring">
      <div class="frame"></div>
     </div>
     <div class="ring">
      <div class="frame"></div>
     </div>
    </div>
   </div>
  </div>
  <!-- Pre-loader end -->
  <div id="pcoded" class="pcoded">
   <div class="pcoded-overlay-box"></div>
   <div class="pcoded-container navbar-wrapper">
    <?php include 'includes/nav.inc.php';?>

    <!-- Sidebar inner chat end-->
    <div class="pcoded-main-container">
     <div class="pcoded-wrapper">
      <?php include 'includes/sidebar.inc.php';?>
      <div class="pcoded-content">
       <div class="pcoded-inner-content">
        <div class="main-body">
         <div class="page-wrapper">
          <div class="page-header">
           <div class="row align-items-end">
            <div class="col-lg-10">
             <div class="page-header-title">
              <div class="d-inline">
               <h4>Add New User</h4>
              </div>
             </div>
            </div>
            <div class="col-lg-2">
             <a href="users-list.php" class="btn btn-block btn-mat btn-primary"><i class="feather icon-list"></i>Users List</a>
            </div>
           </div>
          </div>
          <div class="page-body">
           <div class="row">
            <div class="col-xl-12 col-md-12">
             <div class="card">
              <div class="card-header">
               <h5>Add User</h5>
              </div>
              <div class="card-block">
               <div class="row">
                <div class="col-md-8">
                 <form id="main" method="post" action="/" novalidate="">
                  <div class="form-group row">
                   <label class="col-sm-2 col-form-label">Full Name <small class="txt_mandatory">(*)</small></label>
                   <div class="col-sm-10">
                    <input type="text" class="form-control" name="name" id="name" placeholder="e.g. Galih Kusuma" />
                    <span class="messages"></span>
                   </div>
                  </div>
                  <div class="form-group row">
                   <label class="col-sm-2 col-form-label">Initial<small class="txt_mandatory">(*)</small></label>
                   <div class="col-sm-4">
                    <input type="text" class="form-control" id="initial" name="initial" placeholder="e.g. GK" />
                    <span class="messages"></span>
                   </div>
                  </div>

                  <div class="form-group row">
                   <label class="col-sm-2 col-form-label">Job Desc<small class="txt_mandatory">(*)</small></label>
                   <div class="col-sm-6">
                    <select id="hello-single" class="form-control">
                     <option value="cheese">-- Select One -- </option>
                     <option value="cheese">Project Lead</option>
                     <option value="cheese">Project Assistant</option>
                     <option value="h">Analyst</option>
                     <option value="c">Programmer</option>
                     <option value="h">Copy Writter</option>
                     <option value="h">UI/UX Developer</option>
                     <option value="h">Testing</option>
                    </select>
                    <span class="messages"></span>
                   </div>
                  </div>

                  <div class="form-group row">
                   <label class="col-sm-2 col-form-label">Email Address<small class="txt_mandatory">(*)</small></label>
                   <div class="col-sm-8">
                    <input type="password" class="form-control" id="password" name="password" placeholder="e.g. galih@lingkar9.com" />
                    <span class="messages"></span>
                   </div>
                  </div>

                  <div class="form-group row">
                   <label class="col-sm-2 col-form-label">Mobile Number<small class="txt_mandatory">(*)</small></label>
                   <div class="col-sm-8">
                    <input type="password" class="form-control" id="password" name="password" placeholder="e.g. 083815515129" />
                    <span class="messages"></span>
                   </div>
                  </div>

                  <div class="form-group row">
                   <label class="col-sm-2 col-form-label"> Password <small class="txt_mandatory">(*)</small></label>
                   <div class="col-sm-10">
                    <input type="password" class="form-control" id="repeat-password" name="repeat-password" placeholder="e.g. Password" />
                    <span class="messages"></span>
                   </div>
                  </div>

                  <div class="form-group row">
                   <label class="col-sm-2 col-form-label">Confirm Password <small class="txt_mandatory">(*)</small></label>
                   <div class="col-sm-10">
                    <input type="password" class="form-control" id="repeat-password" name="repeat-password" placeholder="e.g. Confirm Password" />
                    <span class="messages"></span>
                   </div>
                  </div>

                  <div class="form-group row">
                   <label class="col-sm-2 col-form-label">User Level<small class="txt_mandatory">(*)</small></label>
                   <div class="col-sm-6">
                    <select id="hello-single" class="form-control">
                     <option value="cheese">-- Select One -- </option>
                     <option value="cheese">Administration</option>
                     <option value="cheese">Officer</option>
                     <option value="cheese">Directur</option>
                    </select>
                    <span class="messages"></span>
                   </div>
                  </div>

                  <div class="row">
                   <label class="col-sm-2 col-form-label">Status <small class="txt_mandatory">(*)</small></label>
                   <div class="col-sm-10">
                    <div class="form-radio">
                     <div class="radio radiofill radio-primary radio-inline">
                      <label>
                       <input type="radio" name="member" value="free" data-bv-field="member" />
                       <i class="helper"></i>Active
                      </label>
                     </div>
                     <div class="radio radiofill radio-primary radio-inline">
                      <label>
                       <input type="radio" name="member" value="personal" data-bv-field="member" />
                       <i class="helper"></i>Inactive
                      </label>
                     </div>
                    </div>
                    <span class="messages"></span>
                   </div>
                  </div>
                  <div class="form-group row mt-3">
                   <label class="col-sm-2"></label>
                   <div class="col-sm-10">
                    <button type="submit" class="btn btn-primary m-b-0">Submit</button>
                   </div>
                  </div>
                 </form>
                </div>
                <div class="col-md-4">
                    <label class="">Foto Profile</label>
                    <div class="pict_rounded">
                      <p>PHOTO</p>
                    </div>
               <div class="upload_wrapper mt-2">
                                  <div class="file-upload-box">
                                    <label for="upload" class="file-upload__label">
                                      <i class="icon-line2-picture"></i> Upload
                                    </label>
                                    <input id="upload" class="file-upload__input" type="file" name="file-upload">
                                  </div>
                                </div>
                </div>
               </div>
              </div>
             </div>
            </div>
           </div>
          </div>
         </div>
        </div>
       </div>
      </div>
     </div>
    </div>
   </div>
  </div>

  <!-- Warning Section Ends -->
  <!-- Required Jquery -->
  <script type="text/javascript" src="components\jquery\js\jquery.min.js"></script>
  <script type="text/javascript" src="components\jquery-ui\js\jquery-ui.min.js"></script>
  <script type="text/javascript" src="components\popper.js\js\popper.min.js"></script>
  <script type="text/javascript" src="components\bootstrap\js\bootstrap.min.js"></script>
  <!-- jquery slimscroll js -->
  <script type="text/javascript" src="components\jquery-slimscroll\js\jquery.slimscroll.js"></script>
  <!-- modernizr js -->
  <script type="text/javascript" src="components\modernizr\js\modernizr.js"></script>
  <!-- amchart js -->

  <script src="assets\js\jquery.mCustomScrollbar.concat.min.js"></script>
  <script type="text/javascript" src="assets\js\SmoothScroll.js"></script>
  <script src="assets\js\pcoded.min.js"></script>
  <!-- Validation js -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.8.3/underscore-min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/moment.min.js"></script>
  <script type="text/javascript" src="assets\pages\form-validation\validate.js"></script>
  <!-- Custom js -->
  <script type="text/javascript" src="assets\pages\form-validation\form-validation.js"></script>

  <script src="assets\js\vartical-layout.min.js"></script>
  <script type="text/javascript" src="assets\js\script.min.js"></script>
 </body>
</html>
