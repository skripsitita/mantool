<nav class="pcoded-navbar">
 <div class="pcoded-inner-navbar main-menu">
  <div class="pcoded-navigatio-lavel">Navigation</div>
  <ul class="pcoded-item pcoded-left-item">
     <li class="">
    <a href="dashboard.php">
     <span class="pcoded-micon"><i class="feather icon-home"></i></span>
     <span class="pcoded-mtext">Dashboard</span>
    </a>
   </li>

    <li class="">
    <a href="client-list.php">
     <span class="pcoded-micon"><i class="feather icon-users"></i></span>
     <span class="pcoded-mtext">Clients</span>
    </a>
   </li>

   <li class="pcoded-hasmenu active pcoded-trigger">
    <a href="javascript:void(0)">
     <span class="pcoded-micon"><i class="feather icon-home"></i></span>
     <span class="pcoded-mtext">Projects</span>
    </a>
    <ul class="pcoded-submenu">
     <li class="active">
      <a href="project-list.php">
       <span class="pcoded-mtext">New Projects</span>
      </a>
     </li>
     <li class="">
      <a href="dashboard-crm.htm">
       <span class="pcoded-mtext">Project Progress</span>
      </a>
     </li>
     <li class=" ">
      <a href="dashboard-analytics.htm">
       <span class="pcoded-mtext">Analytics</span>
       <span class="pcoded-badge label label-info">NEW</span>
      </a>
     </li>
    </ul>
   </li>

    <li class="">
    <a href="sample-page.htm">
     <span class="pcoded-micon"><i class="feather icon-calendar"></i></span>
     <span class="pcoded-mtext">Activity Schedule</span>
    </a>
   </li>


   <li class="">
    <a href="navbar-light.htm">
     <span class="pcoded-micon"><i class="feather icon-image"></i></span>
     <span class="pcoded-mtext">Portofolio</span>
    </a>
   </li>

   <li class="">
    <a href="navbar-light.htm">
     <span class="pcoded-micon"><i class="feather icon-file"></i></span>
     <span class="pcoded-mtext">Weekly Report</span>
    </a>
   </li>

  
  </ul>
  <div class="pcoded-navigatio-lavel">Master Data</div>

  <ul class="pcoded-item pcoded-left-item">
   <li class="pcoded-hasmenu">
    <a href="javascript:void(0)">
     <span class="pcoded-micon"><i class="feather icon-box"></i></span>
     <span class="pcoded-mtext">Master Data</span>
    </a>
    <ul class="pcoded-submenu">
     <li class="">
      <a href="invoice.htm">
       <span class="pcoded-mtext">Apps Type </span>
      </a>
     </li>
     <li class="">
      <a href="invoice-summary.htm">
       <span class="pcoded-mtext">Apps Category </span>
      </a>
     </li>
     <li class="">
      <a href="invoice-list.htm">
       <span class="pcoded-mtext"> Schedule Type </span>
      </a>
     </li>

       <li class="">
      <a href="invoice-list.htm">
       <span class="pcoded-mtext"> Division</span>
      </a>
     </li>

     <li class="">
      <a href="invoice-list.htm">
       <span class="pcoded-mtext"> Status </span>
      </a>
     </li>
    </ul>
   </li>
  
  
  </ul>
  <div class="pcoded-navigatio-lavel">Users Management</div>
  
  <ul class="pcoded-item pcoded-left-item">
   <li class="pcoded-hasmenu">
    <a href="javascript:void(0)">
     <span class="pcoded-micon"><i class="feather icon-user"></i></span>
     <span class="pcoded-mtext">Users</span>
    </a>
    <ul class="pcoded-submenu">
     <li class="">
      <a href="users-list.php">
       <span class="pcoded-mtext">Users List</span>
      </a>
     </li>
     <li class="">
      <a href="users-level.php">
       <span class="pcoded-mtext">Users Level </span>
      </a>
     </li>
    

    </ul>
   </li>
  
  
  </ul>
 
 </div>
</nav>
